//jshint esversion:6

const express = require("express");
const bodyParser = require("body-parser");
const date = require(__dirname + "/date.js");
const _ = require("lodash");
const mongoose = require("mongoose");

const app = express();
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static("public"));

mongoose.connect('mongodb://127.0.0.1:27017/todolistDB', {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).then(() => {
  console.log('Connected to MongoDB');
}).catch(error => {
  console.error('Error connecting to MongoDB:', error);
});

const itemSchema = new mongoose.Schema({
  name: String
});
const listSchema = new mongoose.Schema({
  name: String,
  items: [itemSchema]
});

const Item = mongoose.model("Item", itemSchema);
const List = mongoose.model("List", listSchema);
const day = date.getDate();
const item1 = new Item({
  name: "Welcome to your todo-list!"
});
const item2 = new Item({
  name: "Click + to add another item."
});
const item3 = new Item({
  name: "<-- Click this to delete an item."
});
const initialItems = [item1, item2, item3];

app.get("/", function(req, res) {
  Item.find().exec().then(function(foundItems){    
    if(foundItems.length === 0){
      Item.insertMany(initialItems).then(function(err){});
      res.redirect("/");
    }else{
      res.render("list", {listTitle: "Today", newListItems: foundItems});
    }
  });
});

app.get("/:index", function(req, res) {
  const listname = _.capitalize(req.params.index);
  List.find({name: listname}).exec().then(function(foundList){    
    if(foundList.length === 0){
      const list1 = new List({
        name: listname,
        items: initialItems
      });
      list1.save();
      res.redirect("/"+listname);
    }
    else{
      res.render("list", {listTitle: listname, newListItems: foundList[0].items});
    }
  });
});

app.post("/", function(req, res){
  const listName = _.capitalize(req.body.submitButton);
  const item = new Item({
    name: req.body.newItem
  });
  if(listName === "Today"){
    item.save();
    res.redirect("/");
  }
  else{
    List.updateOne({name: listName}, {$push: {items : item}}).exec().then(function(){
      res.redirect("/"+listName);
    })
  }
  
});

app.post("/delete", function(req, res){
  const listName = _.capitalize(req.body.hiddenBtn);
  if(listName==="Today"){
    Item.findByIdAndDelete(req.body.deleteBtn).exec().then(function(doc){
      console.log("Deleted successfully");
      res.redirect("/");
    })
  }
  else{
    List.updateOne({name: listName}, {$pull: {items: {_id:req.body.deleteBtn}}}).exec().then(function(){
      res.redirect("/"+listName);
    })
  }
});

app.post("/add", function(req, res){
  const result = _.capitalize(req.body.newListName);
  res.redirect("/"+result);
});

app.get("/about", function(req, res){
  res.render("about");
});

app.listen(3000, function() {
  console.log("Server started on port 3000");
});
